import { Comandante } from './../../modelos/comandante';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { NotifierService } from 'angular-notifier';
import { CookieService } from 'ngx-cookie-service';

import { validarFormulario } from 'src/app/utilitarios/formularios';
import { SalaService } from './../../servicos/sala.service';
import { JogadorService } from './../../servicos/jogador.service';
import { LoginService } from './../../servicos/login.service';
import { Jogador, sjVivo } from './../../modelos/jogador';
import { Resposta, trMensagem, trJogadores } from './../../modelos/resposta';
import { Login, TipoLogin } from './../../modelos/login';
import { CONST_ERRO_DESCONHECIDO_WS } from './../../app-consts';

@Component({
    selector: 'app-monitor',
    templateUrl: './monitor.component.html',
    styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit, OnDestroy {

    /**
     * Título a ser mostrado na tela de login e comando.
     */
    titulo = 'Magic Commander Desktop';

    /**
     * Objeto de formulário que contém os dados de entrada do jogador na sala de combate.
     */
    login: FormGroup;

    /**
     * Inidica se a tela está mostrando ou não uma animação de espera, que indica comunicação com o servidor.
     */
    mostrarEspera = false;

    /**
     * Guarda o valor da aba selecionada entre as abas de edição de jogador e monitoramento de adversários.
     */
    visaoSelecionada = 0;

    /**
     * Nome para um jogador novo a ser criado na sala no modo de monitoramento.
     */
    novoJogadorNome: FormControl = new FormControl('');

    /**
     * Permite subordinar todas as inscrições de monitoramento de
     * mudanças a um único objeto do tipo Subject, para permitir
     * cancelar todas as incrições ao fim do uso do componente,
     * para evitar vazamento de memória na aplicação.
     */
    prescricao: Subject<any>;

    /**
     * Construtor do componente.
     * @param _formBuilder serviço que constrói e administra objetos de formulários
     */
    constructor(
        private formBuilderService: FormBuilder,
        private notificationService: NotifierService,
        private cookieService: CookieService,
        private loginService: LoginService,
        private jogadorService: JogadorService,
        private salaService: SalaService,
    ) {
        this.prescricao = new Subject(); // Inicializa o controle de cancelamento de inscrições
    }

    /**
     * Método executado assim que o componente é inicializado na pilha do Angular antes de ser mostrado para o usuário.
     */
    ngOnInit() {
        // Inicializa o formulário do jogador na sala.
        this.login = this.formBuilderService.group(
            {
                sala: [this.cookieService.get('sala'), Validators.required],
                senha: [this.cookieService.get('senha')]
            }
        );
    }

    /**
     * Implementa a interface ngOnDestroy para permitir cancelar as
     * inscrições de monitoramento abertas durante o uso do componente.
     */
    ngOnDestroy(): void {
        this.prescricao.next();
        this.prescricao.complete();
    }

    /**
     * Altera o modo de exibição da tela, incluindo a animação de espera ou retirando-a.
     */
    alternarEspera(modo: boolean = true): void {
        this.mostrarEspera = modo;
    }

    /**
     * Verifica a validade do formulário de login na sala de jogo.
     */
    validarLogin(): void {
        validarFormulario(this.login);
    }

    /**
     * Ação padrão para erros de preenchimento de login
     */
    erroPreenchimentoLogin(): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', 'Preencha os campos obrigatórios');
    }

    /**
     * Inicia os procedimentos para criar uma sala ou apenas entrar nela.
     */
    monitorar(acao: TipoLogin): void {
        this.alternarEspera();
        this.validarLogin();
        if (this.login.valid) {
            this.loginService.entrar(acao, this.login.value as Login)
                .pipe(takeUntil(this.prescricao))
                .subscribe(
                    (resposta: Resposta) => this.processarRespostaLogin(resposta),
                    (erro: any) => this.processarFalhaLogin(erro)
                );
        } else {
            this.erroPreenchimentoLogin();
        }
    }

    /**
     * Procedimentos tomados após receber a resposta do servidor.
     */
    processarRespostaLogin(resposta: Resposta): void {
        this.alternarEspera(false);
        // Resposta Positiva
        if ((resposta.tipo === trMensagem && resposta.sucesso === true) || resposta.tipo === trJogadores) {
            if (resposta.tipo === trMensagem) {
                this.notificationService.notify('success', resposta.conteudo as string);
                this.atualizarSala();
            }
            // Obtenção do objeto jogados a partir de uma cópia do banco dados.
            if (resposta.tipo === trJogadores) {
                this.salaService.inserirJogadores(resposta.conteudo as Jogador[]);
            }
            this.salaService.ativarCicloAutomatizado(true);
            this.salaService.iniciarCicloAtualizacao();
        }
        // Resposta Negativa
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
    }

    /**
     * Procedimentos tomados caso ocorra um erro ao tentar acessar o servidor de banco de dados.
     */
    processarFalhaLogin(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message) ;
        } else {
            console.log('Erro:', erro);
        }
    }

    /**
     * Retorna os dados de login já realizados em uma sala.
     */
    get Login(): Login {
        return this.loginService.Login;
    }

    /**
     * Retorna os dados dos jogadores da sala.
     */
    get Jogadores(): Jogador[] {
        return this.salaService.Jogadores;
    }

    /**
     * Atualiza o conteúdo da sala, obtendo os dados de todos os jogadores,
     * podendo ser chamado pelo jogador tanto ao trocar de visão, como ao usar o botão.
     */
    atualizarSala(): void {
        this.alternarEspera();
        this.salaService.atualizar()
            .pipe(takeUntil(this.prescricao))
            .subscribe(
                (resposta: Resposta) => this.processarRespostaJogadores(resposta),
                (erro: any) => this.processarRespostaJogadores(erro)
            );
    }

    /**
     * Processa a resposta à atualização da sala, que pode ser positiva ou negativa, mas tratada mesmo é pelo serviço.
     */
    processarRespostaJogadores(resposta: Resposta | any): void {
        this.alternarEspera(false);
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
        if (resposta.tipo === trJogadores) {
            this.salaService.ativarCicloAutomatizado(true);
        }
    }

    /**
     * Processa erros desconhecidos na resposta à atualização da sala.
     */
    processarFalhaJogadores(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message);
        } else {
            console.log('Erro:', erro);
        }
    }

    /**
     * Cria um novo jogador na sala de combate.
     */
    criarNovoJogador(vida: number): void {
        if (this.novoJogadorNome.value.trim() === '') {
            this.notificationService.notify('error', 'Digite o nome do novo jogador');
            return;
        }
        const novoJogador: Jogador = new Jogador();
        let podeCriar = true;
        if (this.Jogadores && this.Jogadores.length > 0) {
            this.Jogadores.forEach((jogador: Jogador) => {
                if (jogador.nome === this.novoJogadorNome.value.trim()) {
                    podeCriar = false;
                }
            });
        }
        if (!podeCriar) {
            this.notificationService.notify('error', 'Já existe jogador com esse nome');
            return;
        }
        novoJogador.nome = this.novoJogadorNome.value.trim();
        novoJogador.vida = vida;
        this.atualizarJogador(novoJogador);
    }

    /**
     * Envia um comando para o servidor, para atualizar ou criar um jogador.
     */
    atualizarJogador(jogador: Jogador): void {
        this.alternarEspera();
        this.jogadorService.atualizar(jogador)
            .pipe(takeUntil(this.prescricao))
            .subscribe(
                (resposta: Resposta) => this.processarRespostaJogador(resposta),
                (erro: any) => this.processarFalhaJogador(erro)
            );
    }

    /**
     * Processa a resposta do servidor depois de criar um novo jogador
     */
    processarRespostaJogador(resposta: Resposta): void {
        this.alternarEspera(false);
        // Resposta Positiva
        if (resposta.tipo === trMensagem && resposta.sucesso === true) {
            this.notificationService.notify('success', resposta.conteudo as string);
            this.novoJogadorNome.setValue('');
            this.atualizarSala();
        }
        // Resposta Negativa
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
    }

    /**
     * Processa o erro de resposta do servidor depois tentar criar um novo jogador
     */
    processarFalhaJogador(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message) ;
        } else {
            console.log('Erro:', erro);
        }
    }

    /**
     * Remove um jogador da sala de combate.
     */
    retirarDaSala(jogador: Jogador): void {
        this.alternarEspera();
        this.salaService.remover(jogador)
            .pipe(takeUntil(this.prescricao))
            .subscribe(
                (resposta: Resposta) => {
                    this.alternarEspera(false);
                    if (resposta.tipo === trMensagem && resposta.sucesso === false) {
                        this.notificationService.notify('error', resposta.conteudo as string);
                    }
                    if (resposta.tipo === trMensagem && resposta.sucesso === true) {
                        this.atualizarSala();
                    }
                },
                (erro: any) => this.processarFalhaJogadores(erro)
            );
    }

    /**
     * Apaga os dados atuais de danos inflingidos por comandantes e começa-os do zero.
     */
    redefinirComandantes(jogador: Jogador): void {
        jogador.danos = [];
        this.Jogadores.forEach((adversario: Jogador) => {
            const comandante: Comandante = new Comandante();
            comandante.nome = adversario.nome;
            comandante.dano = 0;
            jogador.danos.push(comandante);
        });
        this.atualizarJogador(jogador);
    }

    reiniciarJogador(jogador: Jogador, vida: number): void {
        jogador.vida = vida;
        jogador.veneno = 0;
        jogador.status = sjVivo;
        this.redefinirComandantes(jogador);
    }

}
