import { Component, OnInit } from '@angular/core';

import { SalaService } from './../../servicos/sala.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor(
    private salaService: SalaService
  ) { }

  ngOnInit() {  }

}
