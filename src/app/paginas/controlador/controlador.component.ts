import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { NotifierService } from 'angular-notifier';
import { CookieService } from 'ngx-cookie-service';

import { validarFormulario } from 'src/app/utilitarios/formularios';

import { Login, alCriar, TipoLogin } from './../../modelos/login';
import { LoginService } from './../../servicos/login.service';
import { Resposta, trMensagem, trJogador, trJogadores } from './../../modelos/resposta';
import { Jogador, sjVivo } from './../../modelos/jogador';
import { JogadorService } from './../../servicos/jogador.service';
import { SalaService } from './../../servicos/sala.service';
import { Comandante } from './../../modelos/comandante';
import { APP_TITLE, CONST_ERRO_DESCONHECIDO_WS } from './../../app-consts';

@Component({
    selector: 'app-controlador',
    templateUrl: './controlador.component.html',
    styleUrls: ['./controlador.component.scss']
})
export class ControladorComponent implements OnInit, OnDestroy {

    /**
     * Título a ser mostrado na tela de login e comando.
     */
    titulo = APP_TITLE;

    /**
     * Indica se o jogador já está dentro de uma sala e operando seu personagem ou se ainda precisa entrar na sala.
     */
    jogadorLogado = false;

    /**
     * Objeto de formulário que contém os dados de entrada do jogador na sala de combate.
     */
    login: FormGroup;

    /**
     * Inidica se a tela está mostrando ou não uma animação de espera, que indica comunicação com o servidor.
     */
    mostrarEspera = false;

    /**
     * Guarda o valor da aba selecionada entre as abas de edição de jogador e monitoramento de adversários.
     */
    visaoSelecionada = 0;

    /**
     * Permite subordinar todas as inscrições de monitoramento de
     * mudanças a um único objeto do tipo Subject, para permitir
     * cancelar todas as incrições ao fim do uso do componente,
     * para evitar vazamento de memória na aplicação.
     */
    prescricao: Subject<any>;

    /**
     * Construtor do componente.
     * @param _formBuilder serviço que constrói e administra objetos de formulários
     */
    constructor(
        private formBuilderService: FormBuilder,
        private notificationService: NotifierService,
        private cookieService: CookieService,
        private loginService: LoginService,
        private jogadorService: JogadorService,
        private salaService: SalaService,
    ) {
        this.prescricao = new Subject(); // Inicializa o controle de cancelamento de inscrições
    }

    /**
     * Método executado assim que o componente é inicializado na pilha do Angular antes de ser mostrado para o usuário.
     */
    ngOnInit() {
        // Inicializa o formulário do jogador na sala.
        this.login = this.formBuilderService.group(
            {
                sala: [this.cookieService.get('sala'), Validators.required],
                senha: [this.cookieService.get('senha')],
                jogador: [this.cookieService.get('jogador'), Validators.required],
                vida: [this.cookieService.get('vida'), Validators.required]
            }
        );
    }

    /**
     * Implementa a interface ngOnDestroy para permitir cancelar as
     * inscrições de monitoramento abertas durante o uso do componente.
     */
    ngOnDestroy(): void {
        this.prescricao.next();
        this.prescricao.complete();
    }

    /**
     * Altera o modo de exibição da tela, incluindo a animação de espera ou retirando-a.
     */
    alternarEspera(modo: boolean = true): void {
        this.mostrarEspera = modo;
    }

    /**
     * Verifica a validade do formulário de login na sala de jogo.
     */
    validarLogin(): void {
        validarFormulario(this.login);
    }

    /**
     * Ação padrão para erros de preenchimento de login
     */
    erroPreenchimentoLogin(): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', 'Preencha os campos obrigatórios');
    }

    /**
     * Inicia os procedimentos para criar uma sala ou apenas entrar nela.
     */
    jogar(acao: TipoLogin): void {
        this.alternarEspera();
        this.validarLogin();
        if (this.login.valid) {
            // O jogador não poderá criar uma sala escolhendo PV diferente de 20, 30 ou 40
            if (acao === alCriar && this.login.get('vida').value === '0') {
                this.alternarEspera(false);
                this.notificationService.notify('error', 'Pontos de vida iniciais inválidos');
                this.notificationService.notify('info', 'Salas novas não têm dados a recuperar');
                return;
            }
            this.loginService.entrar(acao, this.login.value as Login)
                .pipe(takeUntil(this.prescricao))
                .subscribe(
                    (resposta: Resposta) => this.processarRespostaLogin(resposta),
                    (erro: any) => this.processarFalhaLogin(erro)
            );
        } else {
            this.erroPreenchimentoLogin();
        }
    }

    /**
     * Procedimentos tomados após receber a resposta do servidor.
     */
    processarRespostaLogin(resposta: Resposta): void {
        this.alternarEspera(false);
        // Resposta Positiva
        if ((resposta.tipo === trMensagem && resposta.sucesso === true) || resposta.tipo === trJogador) {
            this.jogadorLogado = true;
            let jogador: Jogador = new Jogador();
            // Construção do objeto jogador a partir dos dados do login.
            if (resposta.tipo === trMensagem) {
                this.notificationService.notify('success', resposta.conteudo as string);
                jogador.nome = this.loginService.Login.jogador;
                jogador.vida = +this.loginService.Login.vida;
                jogador.status = sjVivo;
            }
            // Obtenção do objeto jogados a partir de uma cópia do banco dados.
            if (resposta.tipo === trJogador) {
                jogador = resposta.conteudo as Jogador;
            }
            this.jogadorService.registrar(jogador);
            this.salaService.iniciarCicloAtualizacao();
        }
        // Resposta Negativa
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
    }

    /**
     * Procedimentos tomados caso ocorra um erro ao tentar acessar o servidor de banco de dados.
     */
    processarFalhaLogin(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message) ;
        } else {
            console.log('Erro:', erro);
        }
    }

    /**
     * Ao trocar de abas, habilita/desabilita a atualização automática de dados de jogadores da sala no backend.
     */
    aoTrocarVisao(evento: MatTabChangeEvent): void {
        if (evento.index === 0) {
            this.salaService.ativarCicloAutomatizado(false);
        } else {
            this.atualizarSala();
        }
    }

    /**
     * Atualiza o conteúdo da sala, obtendo os dados de todos os jogadores,
     * podendo ser chamado pelo jogador tanto ao trocar de visão, como ao usar o botão.
     */
    atualizarSala(): void {
        this.alternarEspera();
        this.salaService.atualizar()
            .pipe(takeUntil(this.prescricao))
            .subscribe(
                (resposta: Resposta) => this.processarRespostaJogadores(resposta),
                (erro: any) => this.processarRespostaJogadores(erro)
            );
    }

    /**
     * Processa a resposta à atualização da sala, que pode ser positiva ou negativa, mas tratada mesmo é pelo serviço.
     */
    processarRespostaJogadores(resposta: Resposta | any): void {
        this.alternarEspera(false);
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
        if (resposta.tipo === trJogadores) {
            this.salaService.ativarCicloAutomatizado(true);
        }
    }

    /**
     * Processa erros desconhecidos na resposta à atualização da sala.
     */
    processarFalhaJogadores(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message);
        } else {
            console.log('Erro:', erro);
        }
    }

    /**
     * Entrega a lista de jogadores para a visão de oponentes da sala.
     */
    get Jogadores(): Jogador[] {
        return this.salaService.Jogadores;
    }

    /**
     * Mostra notificações, cada uma com um comandante e o dano que deu no jogador escolhido.
     */
    listarDanos(jogador: Jogador): void {
        if (jogador.danos === undefined) {
            return;
        }
        if (jogador.danos.length === 0) {
            this.notificationService.notify('info', 'Não há dano de comandante nele') ;
        }
        jogador.danos.forEach((adversario: Comandante) => {
            this.notificationService.notify('info', adversario.nome +  ' já casou ' + adversario.dano + ' de dano') ;
        });
    }

    /**
     * Permite retirar um jogador da sala.
     */
    retirarDaSala(jogador: Jogador): void {
        this.alternarEspera();
        this.salaService.remover(jogador)
            .pipe(takeUntil(this.prescricao))
            .subscribe(
                (resposta: Resposta) => {
                    this.alternarEspera(false);
                    if (resposta.tipo === trMensagem && resposta.sucesso === false) {
                        this.notificationService.notify('error', resposta.conteudo as string);
                    }
                },
                (erro: any) => this.processarFalhaJogadores(erro)
            );
    }

    /**
     * Substituir o jogador que está sendo editado no formulário principal por outro da lista.
     */
    editarAdversario(jogador: Jogador): void {
        this.jogadorService.registrar(jogador);
        this.visaoSelecionada = 0;
    }

}
