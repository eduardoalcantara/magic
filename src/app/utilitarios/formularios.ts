import { FormGroup } from '@angular/forms';

/**
 * Itera em todos os controles do formulário para estimulá-los
 * a verificar se cumprem os seus validadores, para mostrar
 * na tela os erros de cada input, caso existam.
 */
export function validarFormulario(form: FormGroup): void {
    Object.keys(form.controls).forEach((key: string) => {
        const abstractControl = form.get(key);
        if (abstractControl instanceof FormGroup) {
            validarFormulario(abstractControl);
        } else {
            abstractControl.markAsTouched();
        }
    });

}

