import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './paginas/inicio/inicio.component';
import { MonitorComponent } from './paginas/monitor/monitor.component';
import { ControladorComponent } from './paginas/controlador/controlador.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: InicioComponent
    },
    {
        path: 'monitor',
        pathMatch: 'full',
        component: MonitorComponent
    },
    {
        path: 'controlador',
        pathMatch: 'full',
        component: ControladorComponent
    },
    {
        path      : '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
