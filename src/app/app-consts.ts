/**
 * Lista de valores constantemente utilizados no sistema.
 */

export const APP_TITLE = 'Magic Commander Desktop';

export const CONST_ERRO_DESCONHECIDO_WS = 'Erro desconhecido no servidor';
