import { trJogadores, trMensagem } from './../modelos/resposta';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { Jogador } from './../modelos/jogador';
import { LoginService } from './login.service';
import { Resposta } from '../modelos/resposta';

@Injectable({
    providedIn: 'root'
})
export class SalaService {

    /**
     * Registro dos magos do jogadores que estão jogando na sala monitorada.
     */
    jogadores: Jogador[];

    /**
     * Determina um ciclo, em segundos, para atualização automática de dados da sala.
     */
    cicloAtualizacao = 10;

    /**
     * Determina se o ciclo de atualização automático está ativado.
     */
    cicloAtivado = false;

    /**
     * Determina se o ciclo já foi registrado por algum componente ou serviço, e se já está executando.
     */
    cicloRegistrado = false;

    /**
     * Construtor do objeto do serviço.
     */
    constructor(
        private httpService: HttpClient,
        private loginService: LoginService
    ) {
        this.obterCicloServidor().subscribe();
    }

    /**
     * Obtem o valor de cicloAtualizacao do servidor, que pode ser diferente,
     * e é mais fácil de ser alterado do que neste sistema, pois precisaria ser recompilado.
     */
    obterCicloServidor(): Observable<Resposta> {
        return this.httpService.get<Resposta>(`${environment.webService}?acao=ciclo`)
            .pipe(
                map((resposta: Resposta) => {
                    if (resposta.tipo === trMensagem && resposta.sucesso === true) {
                        this.cicloAtualizacao = + (resposta.conteudo as string);
                        return resposta;
                    }
                })
            );
        }

    /**
     * Retorna a lista de jogadores.
     */
    get Jogadores(): Jogador[] {
        return this.jogadores;
    }

    /**
     * Atualiza a lista de jogadores da sala com os dados mais atuais do BD.
     */
    atualizar(): Observable<Resposta> {
        const login = this.loginService.Login;
        const parametros = `?acao=monitorar&sala=${login.sala}&senha=${login.senha}`;
        return this.httpService
            .get<Resposta>(environment.webService + parametros)
            .pipe(
                map((resposta: Resposta) => {
                    // Verifica se a resposta é positiva para então aproveitar seus dados e atualizá-los no serviço.
                    if (resposta.tipo === trJogadores) {
                        this.jogadores = resposta.conteudo as Jogador[];
                    }
                    return resposta;
                })
            );
    }

    /**
     * Preenche a lista de jogadores com dados externos.
     */
    inserirJogadores(jogadores: Jogador[]): void {
        this.jogadores = jogadores;
    }

    /**
     * Solicita ao servidor que remova um jogador da sala de combate.
     */
    remover(jogador: Jogador): Observable<Resposta> {
        const login = this.loginService.Login;
        const parametros = `?acao=remover&sala=${login.sala}&senha=${login.senha}&jogador=${jogador.nome}`;
        return this.httpService
            .get<Resposta>(environment.webService + parametros)
            .pipe(
                map((resposta: Resposta) => {
                    // Verifica se a resposta é positiva para então aproveitar seus dados e atualizá-los no serviço.
                    if (resposta.tipo === trJogadores) {
                        this.jogadores = resposta.conteudo as Jogador[];
                    }
                    return resposta;
                })
            );
    }

    /**
     * Inicia e controla o ciclo de atualização automatizada de dados dos jogadores da sala atual.
     */
    iniciarCicloAtualizacao(): void {
        if (this.cicloRegistrado) {
            return;
        } else {
            this.cicloRegistrado = true;
        }
        setInterval(() => {
            if (this.cicloAtivado && this.loginService.Login !== undefined) {
                this.atualizar()
                    .subscribe();
            }
        }, this.cicloAtualizacao * 1000);
    }

    /**
     * Ativa ou desativa o ciclo automatizado de atualização de dados da sala,
     * feito quando o jogador entra e sai da tela de adversários.
     */
    ativarCicloAutomatizado(ativar: boolean): void {
        this.cicloAtivado = ativar;
    }

}
