import { trMensagem, trJogador } from './../modelos/resposta';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Jogador } from './../modelos/jogador';
import { LoginService } from './login.service';
import { Resposta } from '../modelos/resposta';

@Injectable({
    providedIn: 'root'
})
export class JogadorService {

    /**
     * Registro do mago do jogador que está operando esta instância do aplicativo.
     */
    jogador: Jogador;

    /**
     * Informa aos assinantes (formulário de jogador) que os dados do jogador estão disponíveis no serviço.
     */
    observador: Subject<Jogador> = new Subject<Jogador>();

    /**
     * Construtor do objeto do serviço.
     */
    constructor(
        private httpService: HttpClient,
        private loginService: LoginService
    ) {}

    /**
     * registra os dados do jogador operador no serviço.
     */
    registrar(jogador: Jogador): void {
        this.jogador = jogador;
        this.observador.next(this.jogador);
    }

    /**
     * Retorna os dados do jogador.
     */
    get Jogador(): Jogador {
        return this.jogador;
    }

    /**
     * Atualiza os dados locais do jogador enviando-os para o servidor salvá-los no BD.
     */
    atualizar(jogador: Jogador): Observable<Resposta> {
        const login = this.loginService.Login;
        const parametros = `?acao=jogador&sala=${login.sala}&senha=${login.senha}` +
                           `&jogador=${jogador.nome}&conteudo=${JSON.stringify(jogador)}`;
        return this.httpService
            .get<Resposta>(environment.webService + parametros)
            .pipe(
                map((resposta: Resposta) => {
                    // Verifica se a resposta é positiva para então aproveitar seus dados e atualizá-los no serviço.
                    if ((resposta.tipo === trMensagem && resposta.sucesso === true) || resposta.tipo === trJogador) {
                        this.jogador = jogador;
                    }
                    return resposta;
                })
            );
    }

}
