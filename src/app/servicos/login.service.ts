import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CookieService } from 'ngx-cookie-service';

import { Login } from './../modelos/login';
import { Resposta } from '../modelos/resposta';
import { trMensagem, trJogador } from './../modelos/resposta';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    /**
     * Guarda os dados do último login efeutado pelo usuário em uma sala de combate.
     */
    login: Login;

    /**
     * Construtor do objeto do serviço.
     */
    constructor(
        private cookieService: CookieService,
        private httpService: HttpClient
    ) { }

    /**
     * Grava os dados de login na sala e dados do jogador, para agilizar as próximas entradas no aplicativo.
     */
    salvarCookies(): void {
        this.cookieService.set('sala', this.login.sala);
        this.cookieService.set('senha', this.login.senha);
        if (this.login.jogador) {
            this.cookieService.set('jogador', this.login.jogador);
            this.cookieService.set('vida', this.login.vida);
        }
    }

    /**
     * Faz uma requisição à API de banco de dados para criar ou recuperar uma sala de jogo e dados de um jogador.
     * @param tipo modo de entrada num jogo, criando uma sala nova ou entrando em uma sala existente.
     * @param dados informações de login e de jogador inseridas na requisição.
     */
    entrar(acao: 'criar' | 'entrar', dados: Login): Observable<Resposta> {
        dados.acao = acao;
        dados = this.ajustarDados(dados);
        let parametros = `?acao=${acao}&sala=${dados.sala}&senha=${dados.senha}`;
        if (dados.jogador) {
            parametros = `${parametros}&jogador=${dados.jogador}&vida=${dados.vida}`;
        }
        // https://stackoverflow.com/questions/35325816/post-json-from-angular-2-to-php
        return this.httpService
            .get<Resposta>(environment.webService + parametros)
            .pipe(
                map((resposta: Resposta) => {
                    // Verifica se a resposta é positiva para então aproveitar seus dados e salvá-los em cookies.
                    if ((resposta.tipo === trMensagem && resposta.sucesso === true) || resposta.tipo === trJogador) {
                        this.login = dados;
                        this.salvarCookies();
                    }
                    return resposta;
                })
            );
    }

    /**
     * Remove espaços em branco das beiras dos campos para não salvá-las no banco de dados.
     */
    ajustarDados(dados: Login): Login {
        dados.sala = dados.sala.trim();
        if (dados.senha) {
            dados.senha = dados.senha.trim();
        }
        if (dados.jogador === undefined) {
            return dados;
        }
        dados.jogador = dados.jogador.trim();
        dados.vida = dados.vida.trim();
        return dados;
    }

    /**
     * Entrega os valores gravados no login, como dados da sala e do jogador.
     */
    get Login(): Login {
        return this.login;
    }

}
