import { NotifierOptions } from 'angular-notifier';

/**
 * Configuração do componente de notificações suspensas na tela.
 */
export const NotifierConfig: NotifierOptions = {
    position: {
        horizontal: {
            position: 'middle',
            distance: 10,
        },
        vertical: {
            position: 'bottom',
            distance: 10,
            gap: 10,
        },
    },
    behaviour: {
        autoHide: 5000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: false,
        stacking: 4,
    },
    animations: {
        enabled: true,
        show: {
            preset: 'slide',
            speed: 600,
            easing: 'ease'
        },
        hide: {
            preset: 'slide',
            speed: 600,
            easing: 'ease',
            offset: 50,
        },
        shift: {
            speed: 600,
            easing: 'ease',
        },
        overlap: 150,
    },
};
