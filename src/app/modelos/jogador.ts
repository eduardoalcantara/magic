import { Marcador } from './marcador';
import { Comandante } from './comandante';

/**
 * Este modelo representa um objeto com as estatísticas de um jogador em uma partida de Magic
 */
export class Jogador {

    nome: string;

    vida = 20;

    veneno = 0;

    danos: Comandante[] = [];

    marcadores: Marcador[] = [];

    status: 'vivo' | 'morto';

}

/**
 * Indica a quantidade máxima de contadores de veneno que um jogador poderá acumular antes de perder o jogo.
 */
export const venenoMaximo = 9;

/**
 * Indica a quantidade máxima de dano que um jogador pode levar de um mesmo comandante acumuladamente antes de perder o jogo.
 */
export const danoMaximo = 20;

/**
 * Indica o status do mago do jogador como estando vivo.
 */
export const sjVivo = 'vivo';

/**
 * Indica o status do mago do jogador como estando morto.
 */
export const sjMorto = 'morto';
