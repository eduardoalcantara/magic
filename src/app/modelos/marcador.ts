/**
 * Este modelo representa uma anotação de uma quantidade de marcadores
 * de um determinado tipo colocadas sobre uma permanente de um jogador
 * durante uma partidade de Magic.
 */
export class Marcador {

    /**
     * Nome da permanente que possui os marcadores.
     */
    carta = '';

    /**
     * Nome do tipo de marcador. Exemplos: +1/+1, -1/-1, Carga, Divindade...
     */
    tipo = '';

    /**
     * Quantidade de marcadores daquele mesmo tipo em cima da permanente.
     */
    quantidade = 0;

}
