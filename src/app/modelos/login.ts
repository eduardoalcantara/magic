/**
 * Este modelo indica os dados de criação de uma sala de jogo e login de um jogador controlador.
 */
export class Login {

    /**
     * Nome da sala que está sendo criada, e deve ser única, pois é a chava exclusiva do banco de dados
     */
    sala: string;

    /**
     * Senha para entrar na sala, não é obrigatória.
     */
    senha?: string;

    /**
     * Nome do jogador que está controlando o personagem nesta instância da aplicação.
     */
    jogador: string;

    /**
     * Quantidade de pontos de vida iniciais do personagem do jogador.
     */
    vida: string;

    /**
     * Indica qual ação o usuário está tomando para entrar no jogo: criar uma sala nova ou entrar numa sala existente.
     */
    acao: 'criar' | 'entrar';

}

/**
 * Indica a ação de criar uma sala nova.
 */
export const alCriar = 'criar';

/**
 * Indica a ação de entrar em uma sala existente.
 */
export const alEntrar = 'entrar';

/**
 * Tipo de dado que corresponde às opções de entrada no jogo: criar uma sala nova ou entrar numa sala existente.
 */
export type TipoLogin = 'criar' | 'entrar';
