/**
 * Este modelo indica o nome de um jogador ou seu comandante no jogo, e a quantidade de dano que causou a um jogador.
 */
export class Comandante {

    /**
     * Nome do comandante, ou do jogador que causou dano com sua carta comandante, no jogador que opera o sistema.
     */
    nome = '';

    /**
     * Quantidade de dano acumulado que o comandante já causou ao jogador.
     */
    dano = 0;

}
