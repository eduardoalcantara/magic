import { Jogador } from './jogador';

/*
 * Este modelo representa uma resposta da API cujas partes podem ser identificadas e tratadas adequadamente pela aplicação.
 */
export class Resposta {

    /**
     * O tipo de resposta vem de acordo com o tipo de classe enviado dentro do campo `conteudo`.
     */
    tipo: 'mensagem' | 'jogador' | 'jogadores';

    /**
     * O sucesso é informando quando o tipo de responsta for uma mensagem e pode indicar o resultado de uma requisição.
     */
    sucesso?: boolean;

    /**
     * O conteúdo é a resposta em si, podendo ser uma mensagem de texto ou um objeto JSON do tipo `jogador` ou `jogadores`.
     */
    conteudo: string | Jogador | Jogador[];

}

/**
 * Indica que o tipo de resposta da API é uma mensagem.
 */
export const trMensagem = 'mensagem';

/**
 * Indica que o tipo de resposta da API é um objeto do tipo `jogador`.
 */
export const trJogador = 'jogador';

/**
 * Indica que o tipo de resposta da API é um vetor de objetos do tipo `jogador`.
 */
export const trJogadores = 'jogadores';
