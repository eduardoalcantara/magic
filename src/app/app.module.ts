import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatSelectModule, MatTabsModule,
         MatTooltipModule, MatDividerModule, MatListModule, MatMenuModule } from '@angular/material';

import 'hammerjs';

import { NotifierModule } from 'angular-notifier';
import { NotifierConfig } from './app.notifier';

import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AnimacaoDeEsperaComponent } from './componentes/animacao-de-espera/animacao-de-espera.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { MonitorComponent } from './paginas/monitor/monitor.component';
import { ControladorComponent } from './paginas/controlador/controlador.component';
import { JogadorComponent } from './componentes/jogador/jogador.component';
import { CartaComponent } from './componentes/carta/carta.component';

const AngularMaterialModules = [
    MatFormFieldModule, MatButtonModule, MatInputModule, MatIconModule, MatSelectModule, MatTabsModule, 
    MatTooltipModule, MatDividerModule, MatListModule, MatMenuModule
];

@NgModule({
    declarations: [
        AppComponent,
        AnimacaoDeEsperaComponent,
        InicioComponent,
        MonitorComponent,
        ControladorComponent,
        JogadorComponent,
        CartaComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AngularMaterialModules,
        AppRoutingModule,
        NotifierModule.withConfig(NotifierConfig),
    ],
    providers: [
        CookieService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
