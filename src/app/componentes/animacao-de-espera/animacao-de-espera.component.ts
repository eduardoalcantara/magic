import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

/**
 * Este componente exibe um spinner para demonstrar que dados
 * estão sendo trocados com o servidor, e ao mesmo tempo evita
 * que o usuário aperte qualquer botão enquanto esse processo ocorre.
 */
@Component({
  selector: 'app-animacao-de-espera',
  templateUrl: './animacao-de-espera.component.html',
  styleUrls: ['./animacao-de-espera.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AnimacaoDeEsperaComponent implements OnInit {

  /**
   * @Input() permite que 'mostrar' seja usado como um atributo
   * na tag: <app-anicacao-de-espera mostrar="true"> ou melhor,
   * ligando e desligando o spinner: <app-anicacao-de-espera [mostrar]="booleanVar">
   */
  @Input()
  mostrar = false;

  constructor() { }

  ngOnInit(): void {
  }

}
