import { trMensagem } from './../../modelos/resposta';
import { Comandante } from './../../modelos/comandante';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { NotifierService } from 'angular-notifier';

import { validarFormulario } from 'src/app/utilitarios/formularios';

import { Jogador } from './../../modelos/jogador';
import { JogadorService } from './../../servicos/jogador.service';
import { Login } from './../../modelos/login';
import { LoginService } from './../../servicos/login.service';
import { Resposta } from 'src/app/modelos/resposta';
import { CONST_ERRO_DESCONHECIDO_WS } from './../../app-consts';

@Component({
    selector: 'app-jogador',
    templateUrl: './jogador.component.html',
    styleUrls: ['./jogador.component.scss']
})
export class JogadorComponent implements OnInit, OnDestroy {

    /**
     * Objeto de dados do formulário do jogador operador desta instância.
     */
    jogador: FormGroup;

    /**
     * Inidica se a tela está mostrando ou não uma animação de espera, que indica comunicação com o servidor.
     */
    mostrarEspera = false;

    /**
     * Permite subordinar todas as inscrições de monitoramento de
     * mudanças a um único objeto do tipo Subject, para permitir
     * cancelar todas as incrições ao fim do uso do componente,
     * para evitar vazamento de memória na aplicação.
     */
    prescricao: Subject<any>;

    /**
     * Construtor do componente.
     * @param _formBuilder serviço que constrói e administra objetos de formulários
     */
    constructor(
        private formBuilderService: FormBuilder,
        private notificationService: NotifierService,
        private jogadorService: JogadorService,
        private loginService: LoginService,
    ) {
        this.prescricao = new Subject(); // Inicializa o controle de cancelamento de inscrições
    }

    /**
     * Método executado assim que o componente é inicializado na pilha do Angular antes de ser mostrado para o usuário.
     */
    ngOnInit() {
        // Monitora a aquisição de um objeto do tipo jogador no serviço de controle de mesmo nome.
        if (this.jogadorService.Jogador) {
            this.criarFormulario(this.jogadorService.Jogador);
        }
        this.jogadorService.observador
            .pipe(takeUntil(this.prescricao))
            .subscribe((jogador: Jogador) => {
                this.criarFormulario(jogador);
            });
    }

    /**
     * Implementa a interface ngOnDestroy para permitir cancelar as
     * inscrições de monitoramento abertas durante o uso do componente.
     */
    ngOnDestroy(): void {
        this.prescricao.next();
        this.prescricao.complete();
    }

    /**
     * Altera o modo de exibição da tela, incluindo a animação de espera ou retirando-a.
     */
    alternarEspera(modo: boolean = true): void {
        this.mostrarEspera = modo;
    }

    /**
     * Monta o formulário de manipulação do mago do jogador.
     */
    criarFormulario(jogador: Jogador): void {
        this.jogador = this.formBuilderService.group({
            nome: [jogador.nome],
            vida: [jogador.vida, Validators.required],
            veneno: [jogador.veneno, Validators.required],
            status: [jogador.status],
            danos: this.formBuilderService.array(this.comandantes(jogador.danos))
        });
    }

    /**
     * Monta um subformulário com dados de dano recebido de um comandante.
     */
    comandante(comandante: Comandante): FormGroup {
        return this.formBuilderService.group({
            nome: [comandante.nome, Validators.required],
            dano: [comandante.dano, Validators.required]
        });
    }

    /**
     * Monta uma lista de subformulários com dados de dano recebido de vários comandantes.
     */
    comandantes(comandantes: Comandante[]): FormGroup[] {
        const form: FormGroup[] = [];
        if (comandantes === undefined || comandantes === null) {
            return [];
        }
        if (comandantes.length === 0) {
            return [];
        }
        comandantes.forEach((comandante: Comandante) => {
            form.push(this.formBuilderService.group({
                nome: [comandante.nome, Validators.required],
                dano: [comandante.dano, Validators.required]
            }));
        });
        return form;
    }

    get Comandantes(): FormArray {
        return this.jogador.get('danos') as FormArray;
    }

    get Login(): Login {
        return this.loginService.Login;
    }

    /**
     * Modifica a quantidade de pontos de vida do jogador
     */
    alterarVida(valor: number): void {
        this.jogador.get('vida').setValue( +this.jogador.get('vida').value + valor );
    }

    /**
     * Modifica a quantidade de pontos de vida do jogador
     */
    alterarVeneno(valor: number): void {
        this.jogador.get('veneno').setValue( +this.jogador.get('veneno').value + valor );
        if (this.jogador.get('veneno').value < 0) {
            this.jogador.get('veneno').setValue(0);
        }
    }

    /**
     * Adiciona um novo subformulário com dados de comandante ao formulário do jogador.
     */
    adicionarComandante(): void {
        (this.jogador.get('danos') as FormArray).push(this.comandante(new Comandante()));
    }

    /**
     * Modifica a quantidade de pontos de dano levado pelo comandante inimigo.
     */
    alterarComandante(indice: number, valor: number): void {
        (this.jogador.get('danos') as FormArray).controls[indice].get('dano').setValue(
            + (this.jogador.get('danos') as FormArray).controls[indice].get('dano').value
            + valor );
        if ((this.jogador.get('danos') as FormArray).controls[indice].get('dano').value < 0) {
            (this.jogador.get('danos') as FormArray).controls[indice].get('dano').setValue(0);
        }
    }

    /**
     * Apaga o registro de um comandante inimigo da lista de danos recebidos pelo jogador.
     */
    apagarComandante(indice: number): void {
        (this.jogador.get('danos') as FormArray).removeAt(indice);
    }

    /**
     * Retorna a quantidade de dano que o jogador já levou de determinado comandante.
     */
    danoComandante(indice: number): number {
        return + (this.jogador.get('danos') as FormArray).controls[indice].get('dano').value;
    }

    /**
     * Verifica a validade do formulário de login na sala de jogo.
     */
    validarPreenchimento(): void {
        validarFormulario(this.jogador);
    }

    /**
     * Ação padrão para erros de preenchimento de login
     */
    erroPreenchimentoJogador(): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', 'Preencha os campos obrigatórios');
    }

    enviarAtualizacao(): void {
        this.alternarEspera();
        this.validarPreenchimento();
        if (this.jogador.valid) {
            this.jogadorService.atualizar(this.jogador.value as Jogador)
                .pipe(takeUntil(this.prescricao))
                .subscribe(
                    (resposta: Resposta) => this.processarResposta(resposta),
                    (erro: any) => this.processarFalha(erro)
                );
        } else {
            this.erroPreenchimentoJogador();
        }
    }

    processarResposta(resposta: Resposta): void {
        this.alternarEspera(false);
        // Resposta Positiva
        if (resposta.tipo === trMensagem && resposta.sucesso === true) {
            this.notificationService.notify('success', resposta.conteudo as string);
        }
        // Resposta Negativa
        if (resposta.tipo === trMensagem && resposta.sucesso === false) {
            this.notificationService.notify('error', resposta.conteudo as string);
        }
    }

    processarFalha(erro: any): void {
        this.alternarEspera(false);
        this.notificationService.notify('error', CONST_ERRO_DESCONHECIDO_WS);
        if (erro.error !== undefined && erro.error.message !== undefined) {
            this.notificationService.notify('info', erro.error.message) ;
        } else {
            console.log('Erro:', erro);
        }
    }

}
