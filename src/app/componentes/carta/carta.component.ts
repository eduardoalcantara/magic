import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

import { Jogador } from './../../modelos/jogador';
import { Comandante } from './../../modelos/comandante';

@Component({
    selector: 'app-carta',
    templateUrl: './carta.component.html',
    styleUrls: ['./carta.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CartaComponent implements OnInit {

    /**
     * Dados de entrada do componente para montar a carta do jogador.
     */
    @Input('jogador') // tslint:disable-next-line:no-input-rename
    jogador: Jogador;

    /**
     * Dado de saída que indica que o jogador deseja ser retirado da sala.
     */
    @Output()
    apagar: EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Dado de saída que indica que o jogador deseja redefinir os danos de comandantes.
     */
    @Output()
    redefinirComandantes: EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Devolve o objeto jogador com alterações de valor de vida, dano, veneno etc.
     */
    @Output()
    saida: EventEmitter<Jogador> = new EventEmitter<Jogador>();

    @Output()
    reiniciar: EventEmitter<number> = new EventEmitter<number>();

    constructor() { }

    ngOnInit() {
    }

    /**
     * Envia um aviso ao parente do componente que o botão de apagar jogador foi preessionado.
     */
    retirarDaSala(): void {
        this.apagar.emit(true);
    }

    /**
     * Envia um aviso ao parente do componente que o botão de redefinir danos de comandantes foi pressionado.
     */
    definirComandantes(): void {
        this.redefinirComandantes.emit(true);
    }

    /**
     * Muda o valor de pontos de vida do jogador;
     */
    mudarVida(valor: number): void {
        this.jogador.vida = this.jogador.vida + valor;
        this.saida.emit(this.jogador);
    }

    /**
     * Muda o valor de pontos de veneno do jogador;
     */
    mudarVeneno(valor: number): void {
        if (this.jogador.veneno === 0 && valor < 0) {
            return;
        }
        this.jogador.veneno = this.jogador.veneno + valor;
        this.saida.emit(this.jogador);
    }

    /**
     * Muda o valor de pontos de dano dano por um comandante que atacou o jogador;
     */
    mudarDano(comandante: Comandante, valor: number): void {
        if (comandante.dano === 0 && valor < 0) {
            return;
        }
        comandante.dano = comandante.dano + valor;
        this.saida.emit(this.jogador);
    }

    /**
     * Reinicia as estatísticas do jogador.
     */
    reiniciarJogador(vida: number): void {
        this.reiniciar.emit(vida);
    }

}
