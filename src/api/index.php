<?php
/**
 * API de Banco de Dados do Sistema Magic Commander Desktop 1.0
 * (c) 2019-2020 Eduardo Alcântara [edu@ufpa.br]
 */
 
/**
 * Cabeçalhos iniciais, para permitir CORS e definição de saída em JSON.
 */
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

/**
 * Indique aqui quantos segundos o aplicativo aguarda para verificar novamente os status dos jogadores na sala.
 */
$CICLO_ATUALIZACAO_SALA = 10 ;

/**
 * Verifica qual ação foi solicitada pelo cliente para encaminhá-lo ao processamento correto.
 */
if (isset($_GET['acao'])) {
	if ($_GET['acao'] === 'criar') { criarSala(); }
	if ($_GET['acao'] === 'entrar') { entrarNaSala(); }
	if ($_GET['acao'] === 'monitorar') { monitorarSala(); }
	if ($_GET['acao'] === 'jogador') { salvarJogador(); }
	if ($_GET['acao'] === 'remover') { removerJogador(); }
	if ($_GET['acao'] === 'ciclo') { obterCiclo(); }
}

exit(false);

// ***** Funções de Processamento ********************************* //

/**
 * Retorna ao cliente o número de segundos que ele deve configurar seu ciclo de atualização de dados da sala.
 */
function obterCiclo() {
	retornaMensagem(true, $GLOBALS['CICLO_ATUALIZACAO_SALA']);
}

/**
 * Conecção com o banco de dados.
 */
function obterBD() {
	try {
			$conn = new PDO('mysql:host=localhost;dbname=eduardo8_magic', 'root', ''); // eduardo8_magic, Mcd@2019
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		} catch(PDOException $e) {
			retornaMensagem(false, $e->getMessage());
		} 
}

/**
 * Obtem os dados de entrada do cliente, como o objeto JSON do jogador.
 */
function obterEntrada() {
	return $GLOBALS['entrada'];
}

function obterJogadorNovo($nome, $vida = 20) {
	return '{ "nome": "' . $nome . '", "vida": ' . $vida . ', "veneno": 0, "danos": [] }';
}

/**
 * Retorna para o cliente um objeto do tipo Resposta contendo uma Mensagem.
 */
function retornaMensagem($sucesso, $conteudo) {
	if ($sucesso == true) { $sucesso = "true"; } else { $sucesso = "false";	}
	exit('{ "tipo": "mensagem", "sucesso": ' . $sucesso . ', "conteudo": "' . $conteudo . '" }');
}

/**
 * Retorna para o cliente um objeto do tipo Resposta contendo um jogador.
 */
function retornaJogador($jogador) {
	exit('{ "tipo": "jogador", "conteudo": ' . $jogador . ' }');
}

/**
 * Retorna para o cliente um objeto do tipo Resposta contendo uma lista de jogadores.
 */
function retornaJogadores($jogadores) {
	exit('{ "tipo": "jogadores", "conteudo": [' . $jogadores . ' ] }');
}

/**
 * Procedimentos de uma requisição para que o jogador possa criar uma sala de jogo.
 */
function criarSala() {
	try {
		// Verifica se a sala já existe.
		$conexao = obterBD();
		$pesquisa = $conexao->prepare('select * from sala where nome = :nome');
		$pesquisa->execute(array('nome' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll();
		if ( count($resultado) ) {
			retornaMensagem(false, 'Já existe sala com este nome');
		}
		// Procedimento para criar a nova sala.
		if ($_GET['senha'] === '') {
			$senha = '';
		} else {
			$senha = MD5($_GET['senha']);
		}
		$sala = 'insert into sala values ("' . $_GET['sala'] . '", "' . $senha . '")';
		if ( $conexao->exec($sala) === false ) {
			retornaMensagem(false, 'Erro ao criar a sala no banco');
		}
		// Verifica se o pedido foi para controle de jogador ou para monitoramento
		if (!isset($_GET['jogador'])) {
			retornaMensagem(true, 'Sala criada com sucesso');
		}
		// Procedimento para criar um jogador dentro da sala 
		$objeto = $conexao->quote(obterJogadorNovo($_GET['jogador'], $_GET['vida']));
		$objeto = trim($objeto, "'");
		$jogador = 'insert into jogador (nome, sala, objeto) values ("' . $_GET['jogador'] . '", "' . $_GET['sala'] . '", "' . $objeto . '")'; 
		if ( $conexao->exec($jogador) === false ) {
			retornaMensagem(true, 'Erro ao criar o jogador no banco');
		}
		retornaMensagem(true, 'Sala e jogador criados com sucesso');
	} catch(PDOException $e) {
		retornaMensagem(false, $e->getMessage());
	}
}

/**
 * Procedimentos de uma requisição para que um jogador possa entrar em uma sala já existente.
 */
function entrarNaSala() {
	try {
		// Verifica se a sala existe.
		$conexao = obterBD();
		$pesquisa = $conexao->prepare('select * from sala where nome = :nome limit 1');
		$pesquisa->execute(array('nome' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) {
			retornaMensagem(false, 'Não existe sala com este nome');
		}
		// Verifica se a senha é válida.
		foreach($resultado as $sala) {
			if (($sala->senha !== MD5($_GET['senha'])) && $_GET['senha'] !== '') {
				retornaMensagem(false, 'Senha inválida para esta sala');
			}
		}
		// Verifica se o pedido foi para controle de jogador ou para monitoramento
		if (!isset($_GET['jogador'])) {
			retornaMensagem(true, 'Entrou na sala');
		}
		// Verifica se o jogador solicitado já existe na sala ou não.
		$pesquisa = $conexao->prepare('select * from jogador where nome = :nome and sala = :sala limit 1');
		$pesquisa->execute(array('nome' => $_GET['jogador'], 'sala' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) { // Este jogador ainda não tem um cadastro na sala.
			if ($_GET['vida'] === "0") { // Jogadores não cadastrados não podem entrar com 0 de vida na sala.
				retornaMensagem(false, 'Pontos de vida iniciais inválidos');
			}
			$objeto = $conexao->quote(obterJogadorNovo($_GET['jogador'], $_GET['vida']));
			$objeto = trim($objeto, "'");
			$jogador = 'insert into jogador (nome, sala, objeto) value ("' . $_GET['jogador'] . '", "' . $_GET['sala'] . '", "' . $objeto . '")'; 
			if ( $conexao->exec($jogador) === false ) {
				retornaMensagem(true, 'Erro ao criar jogador no banco');
			}
			retornaMensagem(true, 'Jogador criado com sucesso');
		} else { // Este jogador já tem um cadastro na sala.
			if ($_GET['vida'] === "0") { // Este jogador quer retornar à sala com os mesmo pontos de vida que tinha antes.
				foreach($resultado as $jogador) {
					retornaJogador($jogador->objeto);
				}
			} else { // Este jogador quer retornar à sala com uma nova pontuação de vida. É necessário atualizar no BD.
				$objeto = $conexao->quote(obterJogadorNovo($_GET['jogador'], $_GET['vida']));
				$objeto = trim($objeto, "'");
				$jogador = 'update jogador set objeto =  "' . $objeto . '" where nome = "' . $_GET['jogador'] . '" and sala =  "' . $_GET['sala'] . '"'; 
				if ( $conexao->exec($jogador) === false ) {
					retornaMensagem(true, 'Erro ao atualizar jogador no banco');
				}
				retornaMensagem(true, 'Jogador atualizado com sucesso');
			}
		}
	} catch(PDOException $e) {
		retornaMensagem(false, $e->getMessage());
	}
}

/**
 * Procedimentos para uma requisição em que um jogador possa atualizar os seus dados numa sala.
 */
function salvarJogador() {
	try {
		// Verifica se a sala existe.
		$conexao = obterBD();
		$pesquisa = $conexao->prepare('select * from sala where nome = :nome limit 1');
		$pesquisa->execute(array('nome' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) {
			retornaMensagem(false, 'Não existe sala com este nome');
		}
		// Verifica se a senha é válida.
		foreach($resultado as $sala) {
			if (($sala->senha !== MD5($_GET['senha'])) && $_GET['senha'] !== '') {
				retornaMensagem(false, 'Senha inválida para esta sala');
			}
		}
		// Verifica se o jogador solicitado já existe na sala ou não.
		$pesquisa = $conexao->prepare('select * from jogador where nome = :nome and sala = :sala limit 1');
		$pesquisa->execute(array('nome' => $_GET['jogador'], 'sala' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		$objeto = $conexao->quote($_GET['conteudo']);
		$objeto = trim($objeto, "'");
		if ( !count($resultado) ) { // Este jogador ainda não tem um cadastro na sala, então cria
			$jogador = 'insert into jogador (nome, sala, objeto) values ("' . $_GET['jogador'] . '", "' . $_GET['sala'] . '", "' . $objeto . '")'; 
			if ( $conexao->exec($jogador) === false ) {
				retornaMensagem(true, 'Erro ao criar jogador no banco');
			}
			retornaMensagem(true, 'Jogador criado com sucesso');		
		}
		// Atualiza as informações do jogador no banco de dados
		$jogador = 'update jogador set objeto =  "' . $objeto . '" where nome = "' . $_GET['jogador'] . '" and sala =  "' . $_GET['sala'] . '"'; 
		if ( $conexao->exec($jogador) === false ) {
			retornaMensagem(true, 'Erro ao atualizar jogador no banco');
		}
		retornaMensagem(true, 'Jogador atualizado com sucesso');		
	} catch (PDOException $e) {
		retornaMensagem(false, $e->getMessage());
	} catch (Exception $x) {
		retornaMensagem(false, $x->getMessage());
	}
}

/**
 * Procedimentos para a requisição de uma lista de jogadores de uma sala
 */
function monitorarSala() {
	try {
		// Verifica se a sala existe.
		$conexao = obterBD();
		$pesquisa = $conexao->prepare('select * from sala where nome = :nome limit 1');
		$pesquisa->execute(array('nome' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) {
			retornaMensagem(false, 'Não existe sala com este nome');
		}
		// Verifica se a senha é válida.
		foreach($resultado as $sala) {
			if (($sala->senha !== MD5($_GET['senha'])) && $_GET['senha'] !== '') {
				retornaMensagem(false, 'Senha inválida para esta sala');
			}
		}
		// Obtem a lista de jogadores no BD.
		$pesquisa = $conexao->prepare('select * from jogador where sala = :sala order by nome');
		$pesquisa->execute(array('sala' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) { // Esta sala está vazia
			retornaMensagem(true, 'A sala ainda está vazia');
		}
		// Converte para lista em JSON.
		$jogadores = '';
		foreach($resultado as $jogador) {
			$jogadores = $jogadores . ', ' . $jogador->objeto;
		}
		$jogadores = substr($jogadores, 1);
		retornaJogadores($jogadores);
	} catch (PDOException $e) {
		retornaMensagem(false, $e->getMessage());
	} catch (Exception $x) {
		retornaMensagem(false, $x->getMessage());
	}
}

/**
 * Procedimentos para a requisição de remoção de um jogador de uma sala.
 */
function removerJogador() {
	try {
		// Verifica se a sala existe.
		$conexao = obterBD();
		$pesquisa = $conexao->prepare('select * from sala where nome = :nome limit 1');
		$pesquisa->execute(array('nome' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) {
			retornaMensagem(false, 'Não existe sala com este nome');
		}
		// Verifica se a senha é válida.
		foreach($resultado as $sala) {
			if (($sala->senha !== MD5($_GET['senha'])) && $_GET['senha'] !== '') {
				retornaMensagem(false, 'Senha inválida para esta sala');
			}
		}
		// Verifica se o jogador solicitado já existe na sala ou não.
		$pesquisa = $conexao->prepare('select * from jogador where nome = :nome and sala = :sala limit 1');
		$pesquisa->execute(array('nome' => $_GET['jogador'], 'sala' => $_GET['sala']));
		$resultado = $pesquisa->fetchAll(PDO::FETCH_OBJ);
		if ( !count($resultado) ) { // Este jogador ainda não tem um cadastro na sala.
			retornaMensagem(false, 'Jogador inexistente nesta sala');
		}
		// Remove o jogador da sala no banco de dados
		$jogador = 'delete from jogador where nome = "' . $_GET['jogador'] . '" and sala =  "' . $_GET['sala'] . '"'; 
		if ( $conexao->exec($jogador) === false ) {
			retornaMensagem(false, 'Erro ao remover jogador da sala');
		}
		// Atualiza a lista de jogadores na sala
		monitorarSala();
	} catch (PDOException $e) {
		retornaMensagem(false, $e->getMessage());
	} catch (Exception $x) {
		retornaMensagem(false, $x->getMessage());
	}
}

?>
