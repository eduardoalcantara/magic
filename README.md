# Magic Commander Desktop

Este projeto tem o objetivo de disponibilizar um aplicativo que permite que jogadores de magic controle seus pontos de vida atraves de seus próprios celulares, podendo também checar os pontos de vida dos adversários, tanto pelo seu celular quanto por um monitor remoto, através de um navegador web que esteja conectado na mesma sala de jogo.

# Como Aplicativo Deve Ser Operado:

No celular: O jogador deverá criar ou entrar em uma sala de jogo, fornecendo um nome e uma senha para esta sala. Depois, deverá criar ou importar seus dados de jogador na sala (caso ele já tenha jogado nesta sala anteriormente). Ele deverá informar seu nome, pontos de vida, dano sofrido pelos outros comandantes, dano de infecção, e opcionalmente, marcadores em cima de suas permanentes. Depois de fazer as modificações de valores, o jogadores deverá clicar em enviar alterações. A qualquer momento ele poderá clicar em sala para ver as estatísticas dos outros jogadores.

No computador: É possível usar um PC para mostrar as estatísticas de todos os jogadores publicamente. Basta entrar com o nome e senha da sala de jogo e os dados serão atualizados a cada 5 segundos no servidor. É possível esconder jogadores que não estejam na sala atualmente.

# Ações do Backend:

 - status - recebe as estatísticas de todos os jogadores da sala
 - sala - criar uma nova sala
 - atualiza - envia os dados de um jogador para a sala, criando um novo registro, caso seja um jogador novo
 - recupera - recebe os dados de um jogados em uma sala, caso ele tenha saído da página por engano e queira voltar sem refazer

# Telas Disponíveis e Caminhos da Aplicação

 1. / - Tela Inicial - Apenas dois botões: 2. Módulo de Monitoramento; 3. Módulo de Jogador
 2. /sala - Salas de Jogos - Nome da Sala, Senha de Acesso e mais um botão: 2.1. Entrar na Sala
 2.1. /monitor - Monitor de Sala de Jogo - Uma tela mostrando as estatísitcas de todos os jogadores da sala em colunas
 3. /entrar - Iniciando no Modo Jogador - Nome da Sala, Senha de Acesso, Nome do Jogador, PV Inicial e um botão: 3.1 Entrar na Sala
 3.1. /jogar - Controle de Jogo - Aqui temos duas abas: 3.2. Jogador; 3.3. Adversários
 3.2. Na aba Jogador, há um formulário para alterar as estatísticas do jogador e enviá-las para o servidor
 3.3. Na aba Adversários há uma lista com os nomes dos adversários e seus pontos de vida. Ao clicar no nome de um adversário, sua estatística completa aparecerá
 4. Ao cair para 0 pontos de vida, 10+ marcadores de veneno, ou 20+ de dano de um mesmo comandante, uma janela de confirmação de perda de jogo aparecerá
 * Ao tentar entrar em uma sala inexistente ou jogar com um jogador inexistente, a API irá automaticamente criar o objeto esperado.

# Angular Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
